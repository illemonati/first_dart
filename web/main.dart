@JS()
library main;


import 'dart:html';
import 'package:http/browser_client.dart';
import 'dart:convert';
import 'package:js/js.dart';
import 'fib.dart';

@JS('js_fib')
external num js_fib(num n);


void main() async {
  String ip = await getIp();
  await js_calc_fib();
  await dart_calc_fib();
  ip = "${jsonDecode(ip)['ip']}";
  querySelector("#ip").text = ip;
}

void js_calc_fib() async{
  InputElement fib_input = querySelector("#js_fib_input");
  fib_input.onInput.listen((e) {
    print(e);
    final output = js_fib(int.parse(fib_input.value));
    querySelector("#js_fib_output").text = output.toString();
  });
}

void dart_calc_fib() async{
  InputElement fib_input = querySelector("#dart_fib_input");
  fib_input.onInput.listen((e) {
    print(e);
    final output = fib(int.parse(fib_input.value));
    querySelector("#dart_fib_output").text = output.toString();
  });
}

Future<String> getIp() async {
  var rez = await BrowserClient().get("https://ip.tioft.tech");
  String ip_json = rez.body;
  return ip_json;
}

